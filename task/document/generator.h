#ifndef DOC_GENERATOR_H
#define DOC_GENERATOR_H

#include <memory>
#include <string>

#include "document.h"

namespace doc {

/**
 * @brief Generic generator interface
 *
 */
struct Generator {
    Generator() = default;
    virtual ~Generator() = default;

    Generator(const Generator &) = delete;
    Generator(Generator &&) = delete;
    Generator &operator=(const Generator &) = delete;
    Generator &operator=(Generator &&) = delete;

    /**
     * @brief Create concrete type of generator.
     *
     * @param type Should accept following types:
     *              xml (all path elements shall be tags, values should be kept as a text)
     *              json (all path elements shall be keys of objects, values should be values of objects or arrays)
     *              yaml (same requirements as for json)
     * @return std::unique_ptr<Generator>
     */
    static std::unique_ptr<Generator> create(const std::string &type);

    /**
     * @brief Generate document in requested format.
     *
     * @param document
     * @return std::string
     */
    std::string process(const Document &document) {
        std::string output = _process(document);
        for (const auto &enc : document.getEncoders()) {
            output = enc->process(output);
        }
        return output;
    }

private:
    virtual std::string _process(const Document &) = 0;
};

} // namespace doc

#endif

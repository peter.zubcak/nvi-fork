#include "document.h"
#include "error.h"
#include <iostream>

namespace doc {

PathParts split_path(std::string_view path_view)
{
   PathParts path_parts;
   std::string path_name;

   while (!path_view.empty()) {
       auto it = path_view.find("/");
       if (it != std::string::npos) {
           path_name = path_view.substr(0, it);
           path_view.remove_prefix(1);
       } else {
           path_name = path_view;
       }
       path_view.remove_prefix(path_name.size());
       path_parts.emplace_back(path_name);
   }

   return path_parts;
}

void Document::add(std::string path, std::string value) {
    auto path_parts = split_path(path);
    Node *n = root_node.get();
    if (n) {
        n = n->find_node(path_parts);
    }

    if (n) {
        n->add_node(path_parts, value);
    } else {
        root_node = std::make_unique<Node>(path_parts, value);
    }
}

void Document::setEncoders(std::vector<std::unique_ptr<Encoder>> enc) {
    encoders = std::move(enc);
}

void Document::drop() {
    root_node.reset();
}

std::string Document::status() const {
    throw Error("not implemented");
}

} // namespace doc

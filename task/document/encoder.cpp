#include "encoder.h"
#include "error.h"
#include <sstream>
#include <algorithm>
#include <iomanip>

namespace doc {

struct LowerCaseEncoder :  Encoder
{
    virtual ~LowerCaseEncoder() = default;

    std::string _process(const std::string &) override;
};

std::string LowerCaseEncoder::_process(const std::string &src)
{
    std::string out;
    std::transform(src.begin(), src.end(), std::back_inserter(out), [](char c) { return tolower(c); } );
    return out;
}


struct UpperCaseEncoder : public Encoder
{
    virtual ~UpperCaseEncoder() = default;

    std::string _process(const std::string &) override;
};

std::string UpperCaseEncoder::_process(const std::string &src)
{
    std::string out;
    std::transform(src.begin(), src.end(), std::back_inserter(out), [](char c) { return toupper(c); } );
    return out;
}

struct HexEncoder : public Encoder
{
    virtual ~HexEncoder() = default;

    std::string _process(const std::string &) override;
};

std::string HexEncoder::_process(const std::string &src)
{
    std::ostringstream o_stream;
    for (auto c : src) {
        o_stream <<  std::hex << std::setfill('0') << std::setw(2) << (int)c;
    }
    return o_stream.str();
}

std::unique_ptr<Encoder> Encoder::create(const std::string &type) {

    if (type == "lowercase" ) {
        return std::make_unique<LowerCaseEncoder>();
    } else if (type == "uppercase") {
        return std::make_unique<UpperCaseEncoder>();
    } else if (type == "hex") {
        return std::make_unique<HexEncoder>();
    } else {
        throw Error("not implemented");
    }
}

} // namespace doc

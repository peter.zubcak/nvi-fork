#ifndef DOC_ENCODER_H
#define DOC_ENCODER_H

#include <memory>
#include <string>

namespace doc {

/**
 * @brief Generic encoder interface
 * 
 */
struct Encoder {
    Encoder() = default;
    virtual ~Encoder() = default;

    Encoder(const Encoder &) = delete;
    Encoder(Encoder &&) = delete;
    Encoder &operator=(const Encoder &) = delete;
    Encoder &operator=(Encoder &&) = delete;

    /**
     * @brief Create concrete type of encoder.
     * 
     * @param type Should accept following types:
     *              lowercase (transform all letters to lowercase)
     *              uppercase (transform all letters to uppercase)
     *              hex (transform all letters to ASCII codes)
     * @return std::unique_ptr<Encoder> 
     */
    static std::unique_ptr<Encoder> create(const std::string &type);

    /**
     * @brief Encode content.
     * 
     * @param content A content to be encoded.
     * @return std::string Encoded content.
     */
    std::string process(const std::string &content) {
        return _process(content);
    }

private:
    virtual std::string _process(const std::string &) = 0;

};

} // namespace doc

#endif

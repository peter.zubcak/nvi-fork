#ifndef DOC_DOCUMENT_H
#define DOC_DOCUMENT_H

#include <string>
#include <vector>
#include <list>
#include "encoder.h"

namespace doc {

typedef std::list<std::string> PathParts;

struct Node
{
   Node() {}
   Node(PathParts path_parts, std::string value) {
      name = path_parts.front();
      path_parts.pop_front();
      add_node(path_parts, value);
   }

   std::string name;

   bool is_leaf() const {
      return child_nodes.empty();
   }

   Node *find_node(PathParts &path_parts)
   {
       if (path_parts.empty()) {
           return nullptr;
       }

       std::string &path_item = path_parts.front();
       if (name == path_item) {
           path_parts.pop_front();
           Node *n = nullptr;
           for (auto &ch : child_nodes) {
               n = ch->find_node(path_parts);
               if (n) {
                   return n;
               }
           }
           if (!n) {
               return this;
           }
       }

       return nullptr;
   }

   void add_node(PathParts path_parts, std::string value) {
       auto n = std::make_unique<Node>();

       if (path_parts.empty()) {
           n->name = value;
       } else {
           n->name = path_parts.front();
           path_parts.pop_front();
           n->add_node(path_parts, value);
       }

       if (n->is_leaf() || (child_nodes.size() == 1 && child_nodes.back()->is_leaf())) {
           child_nodes.clear();
       }
       child_nodes.push_back(std::move(n));
   }

   std::vector<std::unique_ptr<Node>> child_nodes;
};

/**
 * @brief Document with dynamically created content.
 *
 */
struct Document {

    /**
     * @brief Add a path with a value into the document.
     * @note If some value is already presented in the document as a part of some key,
     *
     * @param path Path in the document
     * @param value Value
     */
    void add(std::string path, std::string value);

    /**
     * @brief Set the Encoders objects which will be applied on all values during exportation.
     *        All previously set encoders will be removed.
     *
     */
    void setEncoders(std::vector<std::unique_ptr<Encoder>>);

    /**
     * @brief Drop any changes already done with the document.
     *
     */
    void drop();

    const std::vector<std::unique_ptr<Encoder>> &getEncoders() const { return encoders; }

    /**
     * @brief Provide human-readable information about the document.
     *
     * @return std::string
     */
    std::string status() const;

    std::unique_ptr<Node> root_node;


    std::vector<std::unique_ptr<Encoder>> encoders;
};

} // namespace doc

#endif

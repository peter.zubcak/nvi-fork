#include "generator.h"
#include "error.h"

namespace doc {

struct XmlGenerator : public Generator {
    virtual ~XmlGenerator() {}

    std::string _process(const Document &) override;

    void printNode(const Node * node, std::string &out);
};

std::string XmlGenerator::_process(const Document &doc) {
    std::string out;

    if (!doc.root_node) {
        return out;
    }

    printNode(doc.root_node.get(), out);
    return out;
}

void XmlGenerator::printNode(const Node *node, std::string &out)
{
    if (node->is_leaf()) {
        out += node->name;
        return;
    }

    out += "<";
    out += node->name;
    out += ">";

    for (auto &c : node->child_nodes) {
        printNode(c.get(), out);
    }

    out += "</";
    out += node->name;
    out += ">";
}

std::unique_ptr<Generator> Generator::create(const std::string &doc) {
    if (doc == "xml") {
      return std::make_unique<XmlGenerator>();
    }
    throw Error("not implemented");
}

} // namespace doc

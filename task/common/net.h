#ifndef PINGER_SERVER_H
#define PINGER_SERVER_H

#include <map>
#include <string_view>
#include <poll.h>

#include "logger.h"
#include "range.h"
#include "socket.h"

namespace net {

struct Socket : log::Enable<Socket>::WithName {
    Socket(common::Address address, common::Socket socket)
        : WithName(address.host() + (address.port() ? ":" + std::to_string(address.port().value()) : ""))
        , _address(std::move(address))
        , _sock(std::move(socket))
    {}

    template<typename Buffer>
    ssize_t peek(Buffer &buffer) const {
        auto r = _sock.peek(buffer);
        here() << "processed " << r << " bytes";
        return r;
    }

    template<typename Buffer>
    ssize_t receive(Buffer &buffer) const {
        auto r = _sock.receive(buffer);
        here() << "processed " << r << " bytes";
        return r;
    }

    template<typename Buffer>
    ssize_t send(Buffer &buffer) const {
        auto r = _sock.send(buffer);
        here() << "processed " << r << " bytes";
        return r;
    }

    const std::string &host() const {
        return _address.host();
    }

    std::optional<int> port() const {
        return _address.port();
    }

    void close() {
        _sock.close();
    }

    pollfd toPoll() const {
        return _sock.toPoll();
    }

    bool isClosed() const {
        return _sock.isClosed();
    }

    Socket accept() const {
        here() << "enter";
        auto [ sock, address ] = _sock.accept();
        return {std::move(address), std::move(sock)};
    }

    void listen(int backlog) const {
        here() << backlog;
        _sock.listen(backlog);
    }

private:
    common::Address _address;
    common::Socket _sock;
};

struct Storage;

struct SocketReference {
    SocketReference(Storage &storage, unsigned id)
        : _storage(&storage)
        , _id(id)
    {}

    unsigned id() const {
        return _id;
    }

    Socket &get();
    bool valid() const;

private:
    Storage *_storage;
    unsigned _id;
};

struct Storage : log::Enable<Storage> {

    Socket *find(unsigned id) {
        here() << id;
        auto s = _sockets.find(id);
        if (s != _sockets.end())
            return &s->second;
        return nullptr;
    }

    const Socket *find(unsigned id) const {
        here() << id;
        auto s = _sockets.find(id);
        if (s != _sockets.end())
            return &s->second;
        return nullptr;
    }

    bool contains(unsigned id) const {
        here() << id;
        auto s = _sockets.find(id);
        return s != _sockets.end();
    }

    bool remove(unsigned id) {
        here() << id;
        return _sockets.erase(id) == 1;
    }

    SocketReference add(Socket socket) {
        here() << _id;
        auto i = _sockets.emplace(_id++, std::move(socket));
        if (!i.second)
            throw std::runtime_error("internal error");

        return {*this, i.first->first};
    }

    std::map<int, SocketReference> toPoll(std::vector<pollfd> &fds) {
        std::map<int, SocketReference> mapping;

        for (auto &[id, s] : _sockets) {
            fds.push_back(s.toPoll());
            mapping.emplace(fds.back().fd, SocketReference{*this, id});
        }
        return mapping;
    }

    std::size_t size() const {
        return _sockets.size();
    }

private:
    unsigned _id = 0;
    std::map<unsigned, Socket> _sockets;
};

inline Socket &SocketReference::get() {
    return *_storage->find(id());
}

inline bool SocketReference::valid() const {
    return _storage->find(id()) != nullptr;
}

struct EventHandler : log::Enable<EventHandler> {
    EventHandler() = default;
    EventHandler(const EventHandler &) = delete;
    EventHandler(EventHandler &&) = delete;
    EventHandler &operator=(const EventHandler &) = delete;
    EventHandler &operator=(EventHandler &&) = delete;
    virtual ~EventHandler() = default;

    void accept(SocketReference reference) {
        here() << reference.id();
        if (reference.valid())
            onAccept(reference.id(), reference.get());
    }

    bool receive(unsigned id, Socket &socket) {
        here() << id;
        if (socket.isClosed()) {
            close(id);
            return false;
        }

        onData(id, socket);
        return true;
    }

    void close(unsigned id) {
        here() << id;
        onClose(id);
    }

private:
    virtual void onAccept(unsigned id, Socket &) = 0;
    virtual void onData(unsigned id, Socket &) = 0;
    virtual void onClose(unsigned id) = 0;
};

struct Server : log::Enable<Server> {

    Server(std::string_view ports);
    Server(std::vector<int> ports);

    void run(EventHandler &);


private:
    void setupSignals();
    void startListening();
    void accept(const std::vector<Socket *> &, EventHandler &);
    auto split(const std::vector<pollfd> &, const std::map<int, SocketReference> &);
    auto preparePollFds();

    std::vector<Socket> _ears;
    common::FileDescriptor _signalListener;
    Storage _clients;
};

} // namespace net

#endif

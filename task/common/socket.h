#ifndef COMMON_SOCKET_H
#define COMMON_SOCKET_H

#include "file-descriptor.h"
#include "resizeable-buffer.h"
#include "system-error.h"

#include <arpa/inet.h>
#include <array>
#include <chrono>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

namespace common {

struct Address {
    Address(std::string host, int port)
        : _host(std::move(host))
        , _port(port)
    {}

    Address(std::string host)
        : _host(std::move(host))
    {}

    Address(std::string_view host)
        : _host(host.begin(), host.end())
    {}

    Address(struct sockaddr_storage &address) {
        std::array<char, 64> buffer;
        void *addressData = nullptr;

        switch (address.ss_family) {
        case AF_INET:
            addressData = &reinterpret_cast<sockaddr_in &>(address).sin_addr;
            _port = reinterpret_cast<sockaddr_in &>(address).sin_port;
            break;

        case AF_INET6:
            addressData = &reinterpret_cast<sockaddr_in6 &>(address).sin6_addr;
            _port = reinterpret_cast<sockaddr_in6 &>(address).sin6_port;
            break;

        case AF_UNIX:
            _host = reinterpret_cast<sockaddr_un &>(address).sun_path;
            break;

        default:
            _host = "unknown address";
            break;
        }

        if (addressData) {
            ::inet_ntop(address.ss_family, addressData, buffer.data(), buffer.size());
            _host = buffer.data();
        }
    }

    sockaddr_storage native(int family) const {
        sockaddr_storage address;
        address.ss_family = family;

        switch (address.ss_family) {
        case AF_INET:
            if (0 > ::inet_pton(address.ss_family, host().c_str(), &reinterpret_cast<sockaddr_in &>(address).sin_addr))
                throw SystemError("inet_pton");
            reinterpret_cast<sockaddr_in &>(address).sin_port = ::htons(port().value());
            break;

        case AF_INET6:
            if (0 > ::inet_pton(address.ss_family, host().c_str(), &reinterpret_cast<sockaddr_in6 &>(address).sin6_addr))
                throw SystemError("inet_pton");
            reinterpret_cast<sockaddr_in6 &>(address).sin6_port = ::htons(port().value());
            break;

        case AF_UNIX:
            std::strncpy(reinterpret_cast<sockaddr_un &>(address).sun_path, host().c_str(), sizeof(sockaddr_un::sun_path));
            break;

        default:
            throw std::runtime_error("unsupported family");
        }
        return address;
    }

    const std::string &host() const {
        return _host;
    }

    std::optional<int> port() const {
        return _port;
    }

private:
    std::string _host;
    std::optional<int> _port;
};

struct Socket : protected FileDescriptor {
protected:
    Socket(int fd, const char *what)
        : FileDescriptor(fd)
    {
        if (!isValid())
            throw SystemError(what);
    }

    static constexpr int Family = AF_INET;

public:
    Socket()
        : FileDescriptor(::socket(Family, SOCK_STREAM, 0))
    {
        if (!isValid())
            throw SystemError("socket");
    }

    using FileDescriptor::close;
    using FileDescriptor::isValid;
    using FileDescriptor::operator bool;
    using FileDescriptor::read;
    using FileDescriptor::toPoll;
    using FileDescriptor::write;

    void swap(Socket &other) {
        static_cast<FileDescriptor &>(*this).swap(other);
    }

    bool isClosed() const {
        if (descriptor() == Invalid)
            return true;
        std::array<char, 1> buffer;
        return 1 != _peek(buffer);
    }

    /**
     * @brief Peek a data from the file descriptor.
     * 
     * @tparam Container Must implement data, size, capacity, and resize methods.
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of filled characters.
     */
    template<
        template<typename...> class Container,
        typename Char,
        typename... Args
    >
    auto peek(Container<Char, Args...> &buffer) const
        -> std::enable_if_t<traits::isChar<Char>
                         && traits::isResizeableContainer<Container<Char, Args...>>,
                    ssize_t>
    {
        return ResizeableBuffer(buffer).shrink(_peek(buffer));
    }

    /**
     * @brief Peek a data from the file descriptor.
     * 
     * @tparam Container Must implement data and size methods.
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer Fills the buffer up to the its size.
     * @return ssize_t The number of filled characters.
     */
    template<
        template<typename...> class Container,
        typename Char,
        typename... Args
    >
    auto peek(Container<Char, Args...> &buffer) const
        -> std::enable_if_t<traits::isChar<Char>
                         && traits::isAcceptableContainer<Container<Char, Args...>>
                         && !traits::isResizeableContainer<Container<Char, Args...>>,
                    ssize_t>
    {
        return _peek(buffer);
    }


    /**
     * @brief Peek a data from the file descriptor.
     * 
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of filled characters.
     */
    template<
        typename Char,
        std::size_t N
    >
    auto peek(std::array<Char, N> &buffer) const
        -> std::enable_if_t<traits::isChar<Char>,
                    ssize_t>
    {
        return _peek(buffer);
    }

    /**
     * @brief Receive a data from the file descriptor.
     * 
     * @tparam Container Must implement data, size, capacity, and resize methods.
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of filled characters.
     */
    template<
        template<typename...> class Container,
        typename Char,
        typename... Args
    >
    auto receive(Container<Char, Args...> &buffer) const
        -> std::enable_if_t<traits::isChar<Char>
                         && traits::isResizeableContainer<Container<Char, Args...>>,
                    ssize_t>
    {
        return ResizeableBuffer(buffer).shrink(_receive(buffer));
    }

    /**
     * @brief Receive a data from the file descriptor.
     * 
     * @tparam Container Must implement data and size methods.
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer Fills the buffer up to the its size.
     * @return ssize_t The number of filled characters.
     */
    template<
        template<typename...> class Container,
        typename Char,
        typename... Args
    >
    auto receive(Container<Char, Args...> &buffer) const
        -> std::enable_if_t<traits::isChar<Char>
                         && traits::isAcceptableContainer<Container<Char, Args...>>
                         && !traits::isResizeableContainer<Container<Char, Args...>>,
                    ssize_t>
    {
        return _receive(buffer);
    }

    /**
     * @brief Receive a data from the file descriptor.
     * 
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of filled characters.
     */
    template<
        typename Char,
        std::size_t N
    >
    auto receive(std::array<Char, N> &buffer) const
        -> std::enable_if_t<traits::isChar<Char>,
                    ssize_t>
    {
        return _receive(buffer);
    }

    /**
     * @brief Send a data to the file descriptor.
     * 
     * @tparam Container Must implement data and size methods.
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer The buffer of data to be written.
     * @return ssize_t The number of written characters.
     */
    template<
        template<typename...> class Container,
        typename Char,
        typename... Args
    >
    auto send(const Container<Char, Args...> &buffer) const
        -> std::enable_if_t<traits::isChar<Char>
                         && traits::isAcceptableContainer<Container<Char, Args...>>,
                    ssize_t>
    {
        return _send(buffer);
    }

    /**
     * @brief Send a data to the file descriptor.
     * 
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @tparam N The capacity of the buffer.
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of written characters.
     */
    template<
        typename Char,
        std::size_t N
    >
    auto send(const std::array<Char, N> &buffer) const
        -> std::enable_if_t<traits::isChar<Char>,
                    ssize_t>
    {
        return _send(buffer);
    }

    template<
        typename Char,
        std::size_t N
    >
    auto send(const Char (&buffer)[N]) const
        -> std::enable_if_t<traits::isChar<Char>,
                    ssize_t>
    {
        return _send(std::basic_string_view(buffer, N));
    }

    Address getAddress() const {
        struct sockaddr_storage address;
        socklen_t size = sizeof(address);

        if (0 > ::getsockname(descriptor(), reinterpret_cast<sockaddr *>(&address), &size))
            throw SystemError("getsockname");
        return address;
    }

    Address getPeerAddress() const {
        struct sockaddr_storage address;
        socklen_t size = sizeof(address);

        if (0 > ::getpeername(descriptor(), reinterpret_cast<sockaddr *>(&address), &size))
            throw SystemError("getpeername");
        return address;
    }

    std::pair<Socket, Address> accept() const {
        struct sockaddr_storage address;
        socklen_t size = sizeof(address);

        return {
            Socket(::accept(descriptor(), reinterpret_cast<sockaddr *>(&address), &size), "accept"),
            Address(address)
        };
    }

    void bind(Address destination) const {
        auto address = destination.native(Family);
        if (0 > ::bind(descriptor(), reinterpret_cast<sockaddr *>(&address), sizeof(address)))
            throw SystemError("bind");
    }

    void connect(Address destination) const {
        auto address = destination.native(Family);
        if (0 > ::connect(descriptor(), reinterpret_cast<sockaddr *>(&address), sizeof(address)))
            throw SystemError("connect");
    }

    void listen(int backlog) const {
        if (0 > ::listen(descriptor(), backlog))
            throw SystemError("listen");
    }

private:
    template<typename Buffer>
    ssize_t _receive(Buffer &buffer) const {
        return _process(buffer, ::recv, "recv");
    }

    template<typename Buffer>
    ssize_t _send(const Buffer &buffer) const {
        return _process(buffer, ::send, "send", MSG_NOSIGNAL);
    }

    template<typename Buffer>
    ssize_t _peek(Buffer &buffer) const {
        return _process(buffer, ::recv, "recv", MSG_PEEK);
    }

    template<typename Buffer, typename Fn>
    ssize_t _process(Buffer &buffer, Fn fn, const char *what, int flags = 0) const {
        do {
            auto rv = fn(descriptor(), buffer.data(), buffer.size(), flags);
            if (rv != -1)
                return rv;
        } while (errno == EINTR);

        switch (errno) {
        case EWOULDBLOCK:
            return 0;

        case ECONNREFUSED:
        case ENOTCONN:
        case EPIPE:
            throw ConnectionError(what);

        default:
            throw SystemError(what);
        }
    }
};

inline void swap(Socket &lhs, Socket &rhs) {
    lhs.swap(rhs);
}

} // namespace common

#endif

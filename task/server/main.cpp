#include <iostream>
#include <fstream>
#include <signal.h>
#include <string>
#include <string_view>
#include <map>
#include <unistd.h>
#include <vector>

#include "../common/net.h"
#include "../document/cli.h"

static const std::string pidPath = "/var/tmp/nvi-server.pid";

int help(int, char **args) {
    std::cout <<
        args[0] << " <command> [options]\n"
        "commands:\n"
        "\thelp\t\t\tprint this help\n"
        "\tstart [port-number]\n"
        "\tstop" <<
        std::endl;
    return 0;
}

struct Handler : net::EventHandler {
    Handler(net::Server &server)
        : _server(server)
    {
        buffer.reserve(512);
    }

private:
    void onAccept(unsigned id, net::Socket &socket) override {
        if (!_documents.emplace(id, doc::Document()).second) {
            socket.send("Sorry bro, invalid ID");
            socket.close();
        }
        socket.send(doc::CLI::welcome);
        socket.send(doc::CLI::prompt);
    }

    void onData(unsigned id, net::Socket &socket) override {
        socket.receive(buffer);

        auto d = _documents.find(id);
        if (d == _documents.end()) {
            here() << "Sorry bro...";
            socket.send("Sorry bro, invalid ID");
            socket.close();
            return;
        }

        auto response = _cli.execute(d->second, buffer);
        socket.send(response);
        socket.send(doc::CLI::prompt);
    }

    void onClose(unsigned id) override {
        _documents.erase(id);
    }

    std::string buffer;
    net::Server &_server;
    doc::CLI _cli;
    std::map<unsigned, doc::Document> _documents;
};

int start(int argc, char **args) {
    if (argc < 3)
        return 1;

    if (::daemon(1, 0))
        throw common::SystemError("daemon");

    log::here() << "pid: " << ::getpid();
    {
        std::ofstream pidFile(pidPath);
        pidFile << ::getpid();
    }

    log::sink.set<std::ofstream>("server.log", std::ios::app | std::ios::out);

    net::Server server(args[2]);
    Handler handler(server);

    server.run(handler);
    return 0;
}

int stop(int argc, char **args) {
    pid_t pid;
    std::ifstream pidFile(pidPath);
    if (pidFile >> pid) {
        ::kill(pid, SIGINT);
        ::unlink(pidPath.c_str());
    }

    return 0;
}

using Command = int(*)(int, char **);

static const std::map<std::string_view, Command> cmds = {
    { "help", help },
    { "start", start },
    { "stop", stop },
};

int main(int argc, char **argv) try {
    std::string_view command = "help";

    if (1 < argc)
        command = argv[1];

    auto c = cmds.find(command);
    if (c != cmds.end())
        return c->second(argc, argv);
    return help(argc, argv);
} catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    log::here() << "Error: " << e.what();
    ::unlink(pidPath.c_str());
    return 255;
}
